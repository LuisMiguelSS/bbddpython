from database.db import obtenerTablas, getTiposDatosTabla, getClientes, getDepartamentos, getEmpleadosClientes, getEmpleadosVehiculos, getVehiculosServicios, updateTabla, obtenerDato, getColumnasTabla, tipoDatoIsString, getTipoDatoCampoEnTabla, getClavePrimariaTabla, eliminarRegistro, insertarRegistro
import pymysql
import os
import string
import colorama
from colorama import Fore, Style

# Atributos
itemsMenuPrincipal = 1
menuSecundarioSeleccionado = 1
tablaSeleccionada = -1
tablas = list()
letras = string.ascii_lowercase
caracterSalida = "Q"

# Funciones

def unpack(elementos):
    if isinstance(elementos, (list, tuple, set)):
        yield from (x for y in elementos for x in unpack(y))
    elif isinstance(elementos, dict):
        yield from (x for item in elementos.items() for y in item for x in unpack(y))
    else:
        yield elementos


def limpiarPantalla():
    os.system('cls' if os.name == 'nt' else 'clear')

def mostrarInfo(contenido):
    print(Fore.CYAN + contenido + Style.RESET_ALL)

def mostrarInfoSeparador(texto1, texto2, separador):
    print(Fore.CYAN + texto1 + Style.RESET_ALL, texto2, separador)

def mostrarMensajeAviso(mensaje):
    print(Fore.YELLOW + str(mensaje) + Style.RESET_ALL)

def mostrarMensajeExito(mensaje):
    print(Fore.GREEN + str(mensaje) + Style.RESET_ALL)

def mostrarMensajeError(mensaje):
    print(Fore.RED + str(mensaje) + Style.RESET_ALL)

def mostrarMenuPrincipal():
    global tablas
    tablas = obtenerTablas()

    if len(tablas) > 0:
        limpiarPantalla()
        mostrarInfo("\n====================\n-       MENÚ       -\n====================\nTablas a elegir:")

        print("1) Consultas predefinidas")

        numeroOpcion = 2  # Empieza en 2 por la opción "Consultas predefinidas"

        # Mostrar tablas
        for tabla in obtenerTablas():
            print("" + str(numeroOpcion) + ") Tabla " + tabla)
            numeroOpcion += 1

    else:
        mostrarMensajeError("No hay tablas disponibles en la base de datos.")


def getConsultas():
    return ["Ver clientes",
            "Ver departamentos",
            "Empleados y vehículos correspondientes",
            "Empleados y clientes atendidos",
            "Vehículos y servicios realizados"]


def getOpciones():
    return ["Añadir dato",
            "Modificar dato",
            "Eliminar dato"]


def mostrarMenuSecundario(opciones):
    mostrarInfo("\nOpciones:\n---------")

    inidiceLetra = 0
    for opcion in opciones:
        print(" " + letras.__getitem__(inidiceLetra) + ") " + opcion)
        inidiceLetra += 1


# MAIN
colorama.init()

try:
    entrada = ""
    while entrada != caracterSalida:
        # MENÚ PRINCIPAL
        mostrarMenuPrincipal()

        # Solicitar elección al usuario
        entrada = input(Fore.CYAN + "\n(i) \"" + caracterSalida +
                        "\" para salir.\n ~Elección (1-" + str(len(tablas)+1) + "): " + Style.RESET_ALL)

        # Comprobar que la entrada sea válida
        if entrada.isdigit():
            if int(entrada) > 0 and int(entrada) <= len(tablas)+1:

                # MENÚS SECUNDARIOS
                numeroOpciones = 0
                # Consultas predefinidas
                if int(entrada) == 1:
                    numeroOpciones = len(getConsultas())
                    mostrarMenuSecundario(getConsultas())
                    menuSecundarioSeleccionado = 1
                # Tabla
                else:
                    tablaSeleccionada = int(entrada)-2
                    mostrarInfo("Tabla \"" + tablas[tablaSeleccionada] + "\"")
                    numeroOpciones = len(getOpciones())
                    mostrarMenuSecundario(getOpciones())
                    menuSecundarioSeleccionado = 2

                # Pedir elección
                entrada = ""
                while entrada != caracterSalida and letras.find(entrada) >= 0 and letras.find(entrada) < numeroOpciones:
                    entrada = input(Fore.CYAN + "\n(i) \"" + caracterSalida +
                                    "\" para salir.\n ~ Selecciona una opción (a-" + letras.__getitem__(numeroOpciones-1) + "): " + Style.RESET_ALL)
                    letraEscrita = letras.__getitem__(letras.find(entrada))

                    # Abrir opción
                    if menuSecundarioSeleccionado == 1:
                        resultadoConsulta = list()

                        if letraEscrita == "a":
                            resultadoConsulta = getClientes()
                        elif letraEscrita == "b":
                            resultadoConsulta = getDepartamentos()
                        elif letraEscrita == "c":
                            resultadoConsulta = getEmpleadosVehiculos()
                        elif letraEscrita == "d":
                            resultadoConsulta = getEmpleadosClientes()
                        elif letraEscrita == "e":
                            resultadoConsulta = getVehiculosServicios()

                        mostrarInfoSeparador('Resultado:', *resultadoConsulta, separador='\n')

                    else:
                        # Añadir
                        if letraEscrita == "a":
                            print("Añadiendo en tabla " + tablas[tablaSeleccionada])

                            nuevosValores = list()
                            for columna in unpack(getColumnasTabla(tablas[tablaSeleccionada])):

                                # Evitar el intento de modificación de la clave primaria
                                for clave in unpack(getClavePrimariaTabla(tablas[tablaSeleccionada])):
                                    while True:
                                        valor = input(" ·" + columna + ": ")

                                        # Si valor ES decimal y tipo dato NO es string O
                                        # si valor NO es decimal y tipo dato ES string
                                        if valor.isdecimal() and not tipoDatoIsString(getTipoDatoCampoEnTabla(columna, tablas[tablaSeleccionada])) \
                                                or not valor.isdecimal() and tipoDatoIsString(getTipoDatoCampoEnTabla(columna, tablas[tablaSeleccionada])):
                                            nuevosValores.append(valor)
                                            break

                            mostrarInfo("El registro " + str(nuevosValores) + " será añadido.")
                            respuesta = input(Fore.CYAN + "¿Está seguro de que desea añadirlo? (Sí/no): " + Style.RESET_ALL)
                            
                            if respuesta.lower() == "sí" or respuesta.lower() == "si" or respuesta.lower() == "s":
                                    
                                # Actualizar
                                if insertarRegistro(tablas[tablaSeleccionada],nuevosValores) == True:
                                    mostrarMensajeExito("Inserción realizada con éxito.")
                            else:
                                mostrarMensajeExito("Inserción candelada.")

                        # Modificar
                        elif letraEscrita == "b":
                            print("Modificando dato en tabla " + tablas[tablaSeleccionada])

                            # Solicitar ID del dato a modificar
                            idAConsultar = input(Fore.CYAN + " -ID del dato a modificar: " + Style.RESET_ALL)
                            resultadoIdAConsultar = obtenerDato(
                                tablas[tablaSeleccionada], str(idAConsultar))

                            # Si no hay resultados, salir
                            if len(resultadoIdAConsultar) == 0:
                                mostrarMensajeAviso(" [Aviso] No hay resultados para ese ID o no se pueden realizar modificaciones sobre la tabla.")
                                continue

                            mostrarInfo("-Editando información original: " + str(*resultadoIdAConsultar))

                            nuevosValores = list()
                            columnasModificadas = list()
                            for columna in unpack(getColumnasTabla(tablas[tablaSeleccionada])):

                                # Evitar el intento de modificación de la clave primaria
                                for clave in unpack(getClavePrimariaTabla(tablas[tablaSeleccionada])):
                                    if columna == clave:
                                        continue

                                    while True:
                                        valor = input(" ·" + columna + ": ")

                                        # Si valor ES decimal y tipo dato NO es string O
                                        # si valor NO es decimal y tipo dato ES string
                                        if valor.isdecimal() and not tipoDatoIsString(getTipoDatoCampoEnTabla(columna, tablas[tablaSeleccionada])) \
                                                or not valor.isdecimal() and tipoDatoIsString(getTipoDatoCampoEnTabla(columna, tablas[tablaSeleccionada])):
                                            nuevosValores.append(valor)
                                            columnasModificadas.append(columna)
                                            break

                            if len(nuevosValores) > 0:
                                mostrarInfo("Los valores " + str(*resultadoIdAConsultar) + " serán sustituidos por " + str(nuevosValores))
                                respuesta = input(Fore.CYAN + "¿Está seguro de que desea realizar el cambio? (Sí/no): " + Style.RESET_ALL)

                                if respuesta.lower() == "sí" or respuesta.lower() == "si" or respuesta.lower() == "s":
                                    # Casteo doblemente a tuple para asegurar el resultado
                                    identificador = tuple(tuple(resultadoIdAConsultar).__getitem__(0)).__getitem__(0)
                                    
                                    # Actualizar
                                    updateTabla(tablas[tablaSeleccionada],columnasModificadas,nuevosValores,identificador)
                                    mostrarMensajeExito("Actualización realizada con éxito.")
                                else:
                                    mostrarMensajeExito("Actualización candelada.")
                            else:
                                mostrarMensajeAviso(" [Aviso] No se puede actualizar nada de esta tabla debido a su estructura.")

                        # Eliminar
                        elif letraEscrita == "c":
                            print("Eliminando en tabla " + tablas[tablaSeleccionada])
                            idAConsultar = input(Fore.CYAN + " -ID del dato a eliminar: " + Style.RESET_ALL)
                            resultadoIdAConsultar = obtenerDato(tablas[tablaSeleccionada], str(idAConsultar))

                            # Si no hay resultados, salir
                            if len(resultadoIdAConsultar) == 0:
                                mostrarMensajeAviso(" [Aviso] No hay resultados para ese ID, no se puede eliminar.")
                                continue

                            mostrarInfo("Los valores " + str(*resultadoIdAConsultar) + " serán eliminados.")
                            respuesta = input(Fore.CYAN + "¿Está seguro de que desea eliminar el registro?\n También se eliminará toda información asociada a él. (Sí/no): " + Style.RESET_ALL)
                            
                            if respuesta.lower() == "sí" or respuesta.lower() == "si" or respuesta.lower() == "s":
                                # Casteo doblemente a tuple para asegurar el resultado
                                identificador = tuple(tuple(resultadoIdAConsultar).__getitem__(0)).__getitem__(0)
                                    
                                # Actualizar
                                eliminarRegistro(tablas[tablaSeleccionada],idAConsultar)
                                mostrarMensajeExito("Eliminación realizada con éxito.")
                            else:
                                mostrarMensajeExito("Eliminación candelada.")

        else:
            continue
except EOFError:
    mostrarMensajeError(" [Error] El valor introducido no es válido.")