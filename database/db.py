import pymysql
import colorama
from colorama import Fore, Style

# Datos de Conexión
DB_HOST = 'localhost'
DB_USER = 'root'
DB_PASS = ''
DB_NAME = 'SotoDistribution'  # Nombre Base de Datos

def mostrarMensajeError(mensaje):
    colorama.init()
    print(Fore.RED + str(mensaje) + Style.RESET_ALL)

# Obtener tablas
def obtenerTablas():
    tablas = list()
    cursor = None
    conexion = None

    try:
        conexion = pymysql.connect(DB_HOST, DB_USER, DB_PASS, DB_NAME)

        cursor = conexion.cursor()
        cursor.execute("USE " + DB_NAME)
        cursor.execute("SHOW TABLES")

        tablas = [tabla for (tabla,) in cursor]

    except:
        mostrarMensajeError(" [Error] Ha ocurrido un error conectando con la base de datos.")
    finally:
        # Cerrar conexiones
        if cursor is not None:
            cursor.close()
        if conexion is not None:
            conexion.close()

    return tablas

# Obtener datos tabla por id y nombre de la tabla
def obtenerDato(nombreTabla, identificador):
    conexion = None
    cursor = None

    try:
        if existeTabla(str(nombreTabla)):
            conexion = pymysql.connect(DB_HOST, DB_USER, DB_PASS, DB_NAME)
            
            cursor = conexion.cursor()
            query = "SELECT * FROM " + str(nombreTabla) + " WHERE "
            
            # Controlar múltiples claves primarias
            clavesPrimarias = [pk for (pk,) in getClavePrimariaTabla(nombreTabla)]
            indiceIdentificador = 0
            if type(identificador) == list or type(identificador) == set and len(list(identificador)) > 0 and len(identificador) == len(list(identificador)):
                for iden in clavesPrimarias:
                    query += iden + " = " + "\'" + str(list(identificador).pop(indiceIdentificador)) + "\'"
                    indiceIdentificador += 1
            elif str(identificador).isdigit():
                query += "" + clavesPrimarias[0] + " = " + str(identificador)
            else:
                query += "" + clavesPrimarias[0] + " = \'" + str(identificador) + "\'"

            cursor.execute(query)

            return cursor.fetchall()
    
    except:
        mostrarMensajeError(" [Error] Ha ocurrido un error conectando con la base de datos.")
    finally:
        # Cerrar conexiones
        if cursor is not None:
            cursor.close()
        if conexion is not None:
            conexion.close()


# Existe tabla
def existeTabla(tabla):
    for tab in obtenerTablas():
        if tab == str(tabla):
            return True

    return False


# Consultas predefinidas menú
def getClientes():
    return obtenerDatosTabla("cliente")


def getDepartamentos():
    return obtenerDatosTabla("departamento")


def getEmpleadosVehiculos():
    conexion = None
    cursor = None

    try:
        if existeTabla("empleado") and existeTabla("vehiculo") and existeTabla("vehiculoempleado"):
            conexion = pymysql.connect(DB_HOST, DB_USER, DB_PASS, DB_NAME)
            query = """
                    SELECT  CONCAT(e.nombre,' ',e.apellidos) as nombrecompleto,
                            CONCAT(v.marca,' ',v.modelo,' (',v.matricula,')') as 'vehiculo'
                    FROM    empleado as e, vehiculo as v, vehiculo_empleado as ve
                    WHERE   e.dniEmpleado = ve.dniEmpleado
                            AND v.matricula = ve.matricula
                    """
            cursor = conexion.cursor()
            cursor.execute(query)
            return cursor.fetchall()
        else:
            mostrarMensajeError(
                " [Error] No se han podido encontrar las tablas necesarias en la base de datos.")
    except:
        mostrarMensajeError(" [Error] Ha ocurrido un error conectando con la base de datos.")
    finally:
        # Cerrar conexiones
        if cursor is not None:
            cursor.close()
        if conexion is not None:
            conexion.close()


def getEmpleadosClientes():
    conexion = None
    cursor = None
    try:
        if existeTabla("empleado") and existeTabla("cliente") and existeTabla("clienteempleado"):
            conexion = pymysql.connect(DB_HOST, DB_USER, DB_PASS, DB_NAME)
            query = """
                    SELECT  CONCAT(e.nombre,' ',e.apellidos) as 'empleado',
                            CONCAT(c.nombre,' ',c.apellidos) as 'cliente' 
                    FROM    cliente as c, empleado as e, clienteempleado as ce
                    WHERE   c.dniCliente = ce.dniCliente AND
                            e.dniEmpleado = ce.dniEmpleado
                    """
            cursor = conexion.cursor()
            cursor.execute(query)
            return cursor.fetchall()
        else:
            mostrarMensajeError(
                " [Error] No se han podido encontrar las tablas necesarias en la base de datos.")
    except:
        mostrarMensajeError(" [Error] Ha ocurrido un error conectando con la base de datos.")
    finally:
        # Cerrar conexiones
        if cursor is not None:
            cursor.close()
        if conexion is not None:
            conexion.close()


def getVehiculosServicios():
    conexion = None
    cursor = None
    try:
        if existeTabla("vehiculo") and existeTabla("servicio") and existeTabla("serviciovehiculo"):
            conexion = pymysql.connect(DB_HOST, DB_USER, DB_PASS, DB_NAME)
            query = """
                    SELECT  CONCAT(v.marca,' (',v.matricula,') - ',v.kilometraje,'km') as 'vehiculo',
                            s.descripcion as 'servicio'
                    FROM    vehiculo as v, servicio as s, serviciovehiculo as sv
                    WHERE   v.matricula = sv.matricula AND
                            s.idServicio = sv.idServicio
                    """
            cursor = conexion.cursor()
            cursor.execute(query)
            return cursor.fetchall()
        else:
            mostrarMensajeError(
                " [Error] No se han podido encontrar las tablas necesarias en la base de datos.")
    except:
        mostrarMensajeError(" [Error] Ha ocurrido un error conectando con la base de datos.")
    finally:
        # Cerrar conexiones
        if cursor is not None:
            cursor.close()
        if conexion is not None:
            conexion.close()


# Obtener datos tabla
def obtenerDatosTabla(tabla):
    conexion = None
    cursor = None

    try:
        if existeTabla(tabla):
            conexion = pymysql.connect(DB_HOST, DB_USER, DB_PASS, DB_NAME)
            cursor = conexion.cursor()
            cursor.execute("SELECT * FROM " + str(tabla))

            return cursor.fetchall()
        else:
            mostrarMensajeError(" [Error] No se ha podido encontrar la tabla \"" +
                str(tabla) + "\" en la base de datos.")
    except:
        mostrarMensajeError(" [Error] Ha ocurrido un error conectando con la base de datos.")
    finally:
        # Cerrar conexiones
        if cursor is not None:
            cursor.close()
        if conexion is not None:
            conexion.close()

# Get columnas tabla
def getColumnasTabla(tabla):
    conexion = None
    cursor = None

    try:
        if existeTabla(tabla):
            conexion = pymysql.connect(DB_HOST, DB_USER, DB_PASS, DB_NAME)
            cursor = conexion.cursor()

            query = "SELECT COLUMN_NAME FROM information_schema.columns WHERE table_schema = \'" + DB_NAME + "\' AND TABLE_NAME = \'" \
                    + str(tabla) + "\' "

            cursor.execute(query)

            return [columna for (columna,) in cursor.fetchall()]

        else:
            mostrarMensajeError(" [Error] No se ha podido encontrar la tabla \"" +
                str(tabla) + "\" en la base de datos.")
    except:
        mostrarMensajeError(" [Error] Ha ocurrido un error conectando con la base de datos.")
    finally:
        # Cerrar conexiones
        if cursor is not None:
            cursor.close()
        if conexion is not None:
            conexion.close()

# Get tipos datos tabla
def getTiposDatosTabla(tabla):
    conexion = None
    cursor = None

    try:
        if existeTabla(tabla):
            conexion = pymysql.connect(DB_HOST, DB_USER, DB_PASS, DB_NAME)
            cursor = conexion.cursor()

            query = "SELECT DATA_TYPE FROM information_schema.columns WHERE table_schema = \'" + DB_NAME + "\' AND TABLE_NAME = \'" \
                 + str(tabla) + "\'"

            cursor.execute(query)

            return [tipoDato for (tipoDato,) in cursor.fetchall()]

        else:
            mostrarMensajeError(" [Error] No se ha podido encontrar la tabla \"" +
                str(tabla) + "\" en la base de datos.")
    except:
        mostrarMensajeError(" [Error] Ha ocurrido un error conectando con la base de datos.")
    finally:
        # Cerrar conexiones
        if cursor is not None:
            cursor.close()
        if conexion is not None:
            conexion.close()


def getTipoDatoCampoEnTabla(campo, tabla):
    conexion = None
    cursor = None

    try:
        if existeTabla(tabla):
            conexion = pymysql.connect(DB_HOST, DB_USER, DB_PASS, DB_NAME)
            cursor = conexion.cursor()

            query = """ SELECT DATA_TYPE
                        FROM information_schema.columns c
                        WHERE table_schema = \'""" + DB_NAME + """\'
                        AND TABLE_NAME = \'""" + str(tabla) + """\'
                        AND COLUMN_NAME = \'""" + str(campo) + """\'"""

            cursor.execute(query)
            resultado = cursor.fetchall()

            if len(resultado) == 0:
                mostrarMensajeError(" [Error] No se ha podido encontrar la tabla \"" +
                str(tabla) + "\" en la base de datos.")

            return [r for (r,) in resultado].pop(0)

        else:
            mostrarMensajeError(" [Error] No se ha podido encontrar la tabla \"" +
                str(tabla) + "\" en la base de datos.")
                
    except:
        print(" [Error] Ha ocurrido un error conectando con la base de datos.")
    finally:
        # Cerrar conexiones
        if cursor is not None:
            cursor.close()
        if conexion is not None:
            conexion.close()


def tipoDatoIsString(tipo):
    # Nota: únicamente NO considero los valores numéricos como texto, fechas y demás sí.
    switcher = {
        "varchar": True,
        "longtext": True,
        "bigint": False,
        "datetime": True,
        "int": False,
        "tinyint": False,
        "decimal": False,
        "blob": True,
        "smallint": False,
        "double": False,
        "char": True,
        "text": True,
        "date": True,
        "timestamp": True,
        "set": True,
        "varbinary": True,
        "enum": True,
        "longblob": True,
        "mediumtext": True,
        "time": True,
        "float": False,
        "year": False
    }
    
    return switcher.get(tipo, False)


def getClavePrimariaTabla(tabla):
    conexion = None
    cursor = None

    try:
        if existeTabla(tabla):
            conexion = pymysql.connect(DB_HOST, DB_USER, DB_PASS, DB_NAME)
            cursor = conexion.cursor()

            query = """ SELECT k.column_name
                        FROM information_schema.table_constraints t
                        JOIN information_schema.key_column_usage k
                        USING(constraint_name,table_schema,table_name)
                        WHERE t.constraint_name='PRIMARY'
                        AND t.table_schema=\'""" + DB_NAME + """'
                        AND k.table_name=\'""" + str(tabla) + """'"""

            cursor.execute(query)

            return cursor.fetchall()

        else:
            mostrarMensajeError(" [Error] No se ha podido encontrar la tabla \"" +
                str(tabla) + "\" en la base de datos.")
    except:
        mostrarMensajeError(" [Error] Ha ocurrido un error conectando con la base de datos.")
    finally:
        # Cerrar conexiones
        if cursor is not None:
            cursor.close()
        if conexion is not None:
            conexion.close()

def insertarRegistro(tabla, nuevoRegistro):
    conexion = None
    cursor = None
    error = False

    try:
        # Comprobar que los valores recibidos sean correctos
        if existeTabla(tabla):

            # Inicializar conexión
            conexion = pymysql.connect(DB_HOST, DB_USER, DB_PASS, DB_NAME)
            cursor = conexion.cursor()

            # Crear consulta dinámica
            query = "INSERT INTO " + tabla
            queryValues = " VALUES ("

            indice = 0
            for dato in nuevoRegistro:
                if indice < len(nuevoRegistro)-1:
                    queryValues += "\'" + str(dato) + "\', "
                else:
                    queryValues += "\'" + str(dato) + "\'"
                
                indice += 1
            
            query += queryValues + ")"

            # Ejecutar consulta
            cursor.execute(query)
            conexion.commit()
        else:
            mostrarMensajeError(" [Error] La tabla indicada (" + str(tabla) + ") no existe.")
            error = True
    
    except Exception as e:
        mostrarMensajeError(" [Error] Ha ocurrido un error conectando con la base de datos. Detalles: " + str(e))
        error = True
    finally:
        # Cerrar conexiones
        if cursor is not None:
            cursor.close()
        if conexion is not None:
            conexion.close()

        return not error

def eliminarRegistro(tabla, identificador):
    conexion = None
    cursor = None

    try:
        # Comprobar que los valores recibidos sean correctos
        if type(tabla == str) and str(tabla).strip() != "":

            # Inicializar conexión
            conexion = pymysql.connect(DB_HOST, DB_USER, DB_PASS, DB_NAME)
            cursor = conexion.cursor()

            # Crear consulta dinámica
            query = "DELETE FROM " + tabla
            queryWhere = " WHERE "

            # SQL - WHERE
            clavesPrimarias = [pk for (pk,) in getClavePrimariaTabla(tabla)]
            indiceIdentificador = 0
            
            if type(identificador) == list or type(identificador) == set and len(list(identificador)) > 0 and len(clavesPrimarias) == len(list(identificador)):
                for iden in clavesPrimarias:

                    if str(identificador).isdigit():
                        queryWhere += " " + iden + " = " + str(list(identificador).pop(indiceIdentificador)) + " "
                    else:
                        queryWhere += " " + iden + " = \'" + str(list(identificador).pop(indiceIdentificador)) + "\' "

                    indiceIdentificador += 1

            elif str(identificador).isdigit():
                queryWhere += " " + clavesPrimarias[0] + " = " + str(identificador) + " "
            else:
                queryWhere += " " + clavesPrimarias[0] + " = \'" + str(identificador) + "\' "
            
            query += queryWhere

            # Ejecutar consulta
            cursor.execute(query)
            conexion.commit()
            
        else:
            mostrarMensajeError(" [Error] Los valores recibidos no son correctos.")

    except Exception as e:
        mostrarMensajeError(" [Error] Ha ocurrido un error conectando con la base de datos. Detalles: " + str(e))
    finally:
        # Cerrar conexiones
        if cursor is not None:
            cursor.close()
        if conexion is not None:
            conexion.close()

def updateTabla(tabla, campos, valores, identificador):
    conexion = None
    cursor = None

    try:
        # Comprobar que los valores recibidos sean correctos
        if type(tabla == str) and str(tabla).strip() != "" \
                and (type(campos) == list or type(campos) == set) and len(campos) > 0 \
            and (type(valores) == list or type(valores) == set)  and len(valores) > 0 \
                and len(campos) == len(valores):

            # Inicializar conexión
            conexion = pymysql.connect(DB_HOST, DB_USER, DB_PASS, DB_NAME)
            cursor = conexion.cursor()

            # Crear consulta dinámica -> UPDATE tabla SET valor = valor WHERE clavePrimaria = clavePrimaria...
            query = "UPDATE " + tabla + " SET "
            queryWhere = " WHERE "

            # SQL - SET
            indice = 0
            for campo in campos:
                if indice > 0:
                    query += ","
                valor = ""
                if tipoDatoIsString(getTipoDatoCampoEnTabla(campo, tabla)):
                    valor = "'" + list(valores)[indice] + "'"
                else:
                    valor =  list(valores)[indice]

                query += " " + campo + " = " + valor + " "
                indice += 1

            # SQL - WHERE
            clavesPrimarias = [pk for (pk,) in getClavePrimariaTabla(tabla)]
            indiceIdentificador = 0
            
            if type(identificador) == list or type(identificador) == set and len(list(identificador)) > 0 and len(clavesPrimarias) == len(list(identificador)):
                for iden in clavesPrimarias:

                    if str(identificador).isdigit():
                        queryWhere += " " + iden + " = " + str(list(identificador).pop(indiceIdentificador)) + " "
                    else:
                        queryWhere += " " + iden + " = \'" + str(list(identificador).pop(indiceIdentificador)) + "\' "

                    indiceIdentificador += 1

            elif str(identificador).isdigit():
                queryWhere += " " + clavesPrimarias[0] + " = " + str(identificador) + " "
            else:
                queryWhere += " " + clavesPrimarias[0] + " = \'" + str(identificador) + "\' "
            
            query += queryWhere

            # Ejecutar consulta
            cursor.execute(query)
            conexion.commit()
            
        else:
            mostrarMensajeError(" [Error] Los valores recibidos no son correctos.\nDeben ser del formato: str,list,list,id; no estar vacíos y tener el mismo número de elementos.")

    except Exception as e:
        mostrarMensajeError(" [Error] Ha ocurrido un error conectando con la base de datos. Detalles: " + str(e))
    finally:
        # Cerrar conexiones
        if cursor is not None:
            cursor.close()
        if conexion is not None:
            conexion.close()