
DROP DATABASE IF EXISTS sotodistribution;

CREATE DATABASE sotodistribution;


USE sotodistribution;

CREATE TABLE cliente (
    dniCliente char(9) NOT NULL,
    nombre varchar(8000),
    apellidos varchar(8000)
);

CREATE TABLE cliente_empleado (
    dniCliente char(9) NOT NULL,
    dniEmpleado char(9) NOT NULL
);

CREATE TABLE Departamento (
    idDepartamento decimal(9,0) NOT NULL,
    nombre varchar(8000)
);

CREATE TABLE empleado (
    dniEmpleado char(9) NOT NULL,
    nombre varchar(8000),
    apellidos varchar(8000),
    fechaNacimiento date,
    idDepartamento decimal(9,0) NOT NULL
);

CREATE TABLE servicio (
    idServicio decimal(9,0) NOT NULL,
    descripcion varchar(8000),
    idCliente char(9) NOT NULL
);

CREATE TABLE servicio_vehiculo (
    idServicio decimal(9,0) NOT NULL,
    matricula char(7) NOT NULL
);

CREATE TABLE vehiculo (
    matricula char(7) NOT NULL,
    marca varchar(8000),
    modelo varchar(8000),
    kilometraje decimal(9,0)
);

CREATE TABLE vehiculo_empleado (
    matricula char(7) NOT NULL,
    dniEmpleado char(9) NOT NULL
);


INSERT INTO cliente (dniCliente, nombre, apellidos) VALUES ('1', 'Miguel', 'Primero');
INSERT INTO cliente (dniCliente, nombre, apellidos) VALUES ('2', 'Miguel', 'Segundo');
INSERT INTO cliente (dniCliente, nombre, apellidos) VALUES ('3', 'Paco', 'Primero');
INSERT INTO cliente (dniCliente, nombre, apellidos) VALUES ('4', 'Paco', 'Segundo');
INSERT INTO cliente (dniCliente, nombre, apellidos) VALUES ('5', 'Eulalia', 'Del Valle');

INSERT INTO cliente_empleado (dniCliente, dniEmpleado) VALUES ('1', '10000000A');
INSERT INTO cliente_empleado (dniCliente, dniEmpleado) VALUES ('2', '20000000B');
INSERT INTO cliente_empleado (dniCliente, dniEmpleado) VALUES ('3', '30000000C');
INSERT INTO cliente_empleado (dniCliente, dniEmpleado) VALUES ('4', '40000000D');
INSERT INTO cliente_empleado (dniCliente, dniEmpleado) VALUES ('5', '50000000E');

INSERT INTO departamento (idDepartamento, nombre) VALUES (1, 'Informática');
INSERT INTO departamento (idDepartamento, nombre) VALUES (2, 'Movilidad');
INSERT INTO departamento (idDepartamento, nombre) VALUES (3, 'Finanzas');
INSERT INTO departamento (idDepartamento, nombre) VALUES (4, 'Marketing');
INSERT INTO departamento (idDepartamento, nombre) VALUES (5, 'Publicidad');

INSERT INTO empleado (dniEmpleado, nombre, apellidos, fechaNacimiento, idDepartamento) VALUES ('10000000A', 'Luis Miguel', 'Soto Sánchez', '2000-09-29', 1);
INSERT INTO empleado (dniEmpleado, nombre, apellidos, fechaNacimiento, idDepartamento) VALUES ('20000000B', 'Lola', 'Índigo', '1988-07-13', 2);
INSERT INTO empleado (dniEmpleado, nombre, apellidos, fechaNacimiento, idDepartamento) VALUES ('30000000C', 'Pablo', 'León', '1990-02-28', 3);
INSERT INTO empleado (dniEmpleado, nombre, apellidos, fechaNacimiento, idDepartamento) VALUES ('40000000D', 'Lucía', 'Sánchez', '2000-12-13', 4);
INSERT INTO empleado (dniEmpleado, nombre, apellidos, fechaNacimiento, idDepartamento) VALUES ('50000000E', 'Macarena', 'De las Rosas', '1999-08-09', 5);

INSERT INTO servicio (idServicio, descripcion, idCliente) VALUES (1, 'Mudanza', '1');
INSERT INTO servicio (idServicio, descripcion, idCliente) VALUES (2, 'Transporte de mercancías peligrosas', '2');
INSERT INTO servicio (idServicio, descripcion, idCliente) VALUES (3, 'Asistencia en distribución', '3');
INSERT INTO servicio (idServicio, descripcion, idCliente) VALUES (4, 'Mudanza', '4');
INSERT INTO servicio (idServicio, descripcion, idCliente) VALUES (5, 'Festival', '5');

INSERT INTO servicio_vehiculo (idServicio, matricula) VALUES (1, '1111AAA');
INSERT INTO servicio_vehiculo (idServicio, matricula) VALUES (2, '2222BBB');
INSERT INTO servicio_vehiculo (idServicio, matricula) VALUES (3, '3333CCC');
INSERT INTO servicio_vehiculo (idServicio, matricula) VALUES (4, '4444DDD');
INSERT INTO servicio_vehiculo (idServicio, matricula) VALUES (5, '5555EEE');

INSERT INTO vehiculo (matricula, marca, modelo, kilometraje) VALUES ('1111AAA', 'Volvo', 'Voyager I', 0);
INSERT INTO vehiculo (matricula, marca, modelo, kilometraje) VALUES ('2222BBB', 'Mercedes', 'Landscape', 100);
INSERT INTO vehiculo (matricula, marca, modelo, kilometraje) VALUES ('3333CCC', 'Volvo', 'Cruise', 3400);
INSERT INTO vehiculo (matricula, marca, modelo, kilometraje) VALUES ('4444DDD', 'MAN', 'NLimits', 5000);
INSERT INTO vehiculo (matricula, marca, modelo, kilometraje) VALUES ('5555EEE', 'Volvo', 'Trilogy', 2300);

INSERT INTO vehiculo_empleado (matricula, dniEmpleado) VALUES ('1111AAA', '10000000A');
INSERT INTO vehiculo_empleado (matricula, dniEmpleado) VALUES ('2222BBB', '20000000B');
INSERT INTO vehiculo_empleado (matricula, dniEmpleado) VALUES ('3333CCC', '30000000C');
INSERT INTO vehiculo_empleado (matricula, dniEmpleado) VALUES ('4444DDD', '40000000D');
INSERT INTO vehiculo_empleado (matricula, dniEmpleado) VALUES ('5555EEE', '50000000E');



ALTER TABLE cliente ADD PRIMARY KEY (dniCliente);
ALTER TABLE cliente_empleado ADD PRIMARY KEY (dniCliente, dniEmpleado);
ALTER TABLE departamento ADD PRIMARY KEY (idDepartamento);
ALTER TABLE empleado ADD PRIMARY KEY (dniEmpleado);
ALTER TABLE servicio ADD PRIMARY KEY (idServicio);
ALTER TABLE servicio_vehiculo ADD PRIMARY KEY (idServicio,matricula);
ALTER TABLE vehiculo ADD PRIMARY KEY (matricula);
ALTER TABLE vehiculo_empleado ADD PRIMARY KEY (matricula, dniEmpleado);

ALTER TABLE cliente_empleado ADD FOREIGN KEY (dniCliente) REFERENCES cliente(dniCliente) ON DELETE CASCADE;
ALTER TABLE cliente_empleado ADD FOREIGN KEY (dniEmpleado) REFERENCES empleado(dniEmpleado) ON DELETE CASCADE;
ALTER TABLE empleado ADD FOREIGN KEY (idDepartamento) REFERENCES departamento(idDepartamento) ON DELETE CASCADE;
ALTER TABLE servicio ADD FOREIGN KEY (idCliente) REFERENCES cliente(dniCliente) ON DELETE CASCADE;
ALTER TABLE servicio_vehiculo ADD FOREIGN KEY (idServicio) REFERENCES servicio(idServicio) ON DELETE CASCADE;
ALTER TABLE servicio_vehiculo ADD FOREIGN KEY (matricula) REFERENCES vehiculo(matricula) ON DELETE CASCADE;
ALTER TABLE vehiculo_empleado ADD FOREIGN KEY (dniEmpleado) REFERENCES empleado(dniEmpleado) ON DELETE CASCADE;
ALTER TABLE vehiculo_empleado ADD FOREIGN KEY (matricula) REFERENCES vehiculo(matricula) ON DELETE CASCADE;